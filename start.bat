@ECHO OFF

IF EXIST "5d-chess-db-win.exe" (
  ECHO "Running Executable"
  START "5D Chess DB Gen" "5d-chess-db-win.exe"
) ELSE (
  ECHO "Running Using NPM"
  START "5D Chess DB Gen" npm run dbgen
)


MKDIR db

MKDIR db_pull

:rsync

ECHO "Pushing to Cloud"

CALL gsutil -m mv -ce .\db\ gs://db-chess-in-5d/

TIMEOUT 3600

GOTO rsync