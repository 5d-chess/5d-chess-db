const blankAction = (chessInstance, board, action) => {
  var presentTimelines = chessInstance.raw.boardFuncs.present(board, action);
  for(var i = 0;i < presentTimelines.length;i++) {
    if(board[presentTimelines[i]]) {
      var currTimeline = board[presentTimelines[i]];
      var latestTurn = currTimeline[currTimeline.length - 1];
      if((currTimeline.length - 1) % 2 === action % 2) {
        var newTurn = chessInstance.raw.turnFuncs.copy(board, presentTimelines[i], currTimeline.length - 1);
        board[presentTimelines[i]][currTimeline.length] = newTurn;
      }
    }
  }
}

const fchecks = (chessInstance, board, action) => {
  var res = [];
  var tmpBoard = chessInstance.raw.boardFuncs.copy(board);
  blankAction(chessInstance, tmpBoard, action);
  var moves = chessInstance.raw.boardFuncs.moves(tmpBoard, action + 1, false, false);
  for(var i = 0;i < moves.length;i++) {
    if(moves[i].length === 2 && chessInstance.raw.boardFuncs.positionExists(tmpBoard, moves[i][1])) {
      var destPiece = tmpBoard[moves[i][1][0]][moves[i][1][1]][moves[i][1][2]][moves[i][1][3]];
      if((Math.abs(destPiece) === 11 || Math.abs(destPiece) === 12) && Math.abs(destPiece) % 2 === action % 2) {
        res.push(moves[i]);
      }
    }
  }
  return res;
}

const checkmate = (chessInstance, board, action, maxTime = 60000) => {
  var start = Date.now();

  // Super fast single pass looking for moves solving checks
  var moves = chessInstance.raw.boardFuncs.moves(board, action, false, false);
  for(var i = 0;i < moves.length;i++) {
    var tmpBoard = chessInstance.raw.boardFuncs.copy(board);
    chessInstance.raw.boardFuncs.move(tmpBoard, moves[i]);
    var tmpChecks = fchecks(chessInstance, tmpBoard, action);
    if(tmpChecks.length <= 0) { return false; }
    if((Date.now() - start) > maxTime) { return true; }
  }
  // Fast pass looking for moves solving checks using DFS
  var recurse = (board, action, checks = []) => {
    var moves = chessInstance.raw.boardFuncs.moves(board, action, false, false);
    if(checks.length <= 0) { checks = fchecks(chessInstance, board, action); }
    if(checks.length <= 0) { return false; }
    if((Date.now() - start) > maxTime) { return true; }
    for(var i = 0;i < moves.length;i++) {
      var tmpBoard = chessInstance.raw.boardFuncs.copy(board);
      chessInstance.raw.boardFuncs.move(tmpBoard, moves[i]);
      var tmpChecks = fchecks(chessInstance, tmpBoard, action);
      var solvedACheck = tmpChecks.length < checks.length;
      if(solvedACheck) {
        if(!recurse(tmpBoard, action, tmpChecks)) {
          return false;
        }
      }
    }
    return true;
  }
  if(!recurse(board, action)) { return false; }

  var checkSig = (checks) => {
    var res = {
      length: checks.length,
      sig: []
    };
    checks.sort(this.moveCompare);
    res.sig = checks.flat(2);
    return res;
  };
  var nodeSort = (n1, n2) => {
    if(n1.checkSig.length !== n2.checkSig.length) {
      return n1.checkSig.length - n2.checkSig.length;
    }
    if(n1.checkSig.sig.length !== n2.checkSig.sig.length) {
      return n1.checkSig.sig.length - n2.checkSig.sig.length;
    }
    for(var i = 0;i < n1.checkSig.sig.length;i++) {
      if(n1.checkSig.sig[i] !== n2.checkSig.sig[i]) {
        return n1.checkSig.sig[i] - n2.checkSig.sig[i];
      }
    }
    return n1.board.length - n2.board.length;
  };

  var exhausted = false;
  var moveTree = [{
    board: board,
    checkSig: checkSig(fchecks(chessInstance, board, action))
  }];
  var moveTreeIndex = 0;
  //Slow BFS exhaustive search prioritizing check solving, check changing, then timeline changing moves
  while(moveTreeIndex < moveTree.length) {
    if((Date.now() - start) > maxTime) { return true; }
    var currNode = moveTree[moveTreeIndex];
    if(currNode) {
      var moves = chessInstance.raw.boardFuncs.moves(currNode.board, action, false, false);
      var tmpMoveTree = [];
      for(var i = 0;i < moves.length;i++) {
        var tmpBoard = chessInstance.raw.boardFuncs.copy(currNode.board);
        chessInstance.raw.boardFuncs.move(tmpBoard, moves[i]);
        var tmpChecks = fchecks(chessInstance, tmpBoard, action);
        if(tmpChecks.length <= 0) { return false; }
        var tmpCheckSig = checkSig(tmpChecks);
        tmpMoveTree.push({
          board: tmpBoard,
          checkSig: tmpCheckSig
        });
      }
      tmpMoveTree.sort((e1, e2) => nodeSort(currNode, e2) - nodeSort(currNode, e1));
      for(var i = 0;i < tmpMoveTree.length;i++) {
        moveTree.push(tmpMoveTree[i]);
      }
      moveTree.splice(0, 1);
      moveTreeIndex--;
    }
    moveTreeIndex++;
  }
  return true;
}

module.exports = (chessInstance) => {
  return (chessInstance.rawMoveBuffer.length <= 0) && checkmate(chessInstance, chessInstance.rawBoard, chessInstance.rawAction, 60000);
};
