const { expose } = require('threads/worker');
const Chess = require('5d-chess-js');
const moment = require('moment');
const bruteCheckmate = require('./bruteCheckmate');
const bench = require('./bench');
const pjson = require('../package.json');

expose({
  gen: () => {
    var res = {
      path: './db/',
      filename: '',
      data: ''
    };
    var chess = new Chess();
    var variant = ['standard', 'defended pawn', 'half reflected', 'princess', 'turn zero'].sort((e1, e2) => 0.5 - Math.random())[0];
    chess.reset(variant);
    chess.checkmateTimeout = 120000;
    res.path += variant.replace(/\s+/g, '-') + '/';
    var timedOut = false;
    while(!timedOut) {
      var action = [];
      var valid = false;
      var start = Date.now();
      while(!valid && !timedOut) {
        action = [];
        var submit = false;
        var tmpChess = chess.copy();
        while(!submit && !timedOut) {
          var moves = tmpChess.moves('object', true, true, true);
          if(moves.length > 0) {
            var move = moves[Math.floor(Math.random() * moves.length)];
            action.push(move);
            tmpChess.move(move);
          }
          else {
            submit = true;
          }
          if((Date.now() - start) > 20000 && !timedOut) {
            timedOut = true;
          }
        }
        if(!tmpChess.inCheck) {
          valid = true;
        }
      }
      if(valid && !timedOut) {
        try {
          chess.action(action, true);
        }
        catch(err) {}
      }
    }
    var bench1 = bench();
    var start = Date.now();
    var checkmate = bruteCheckmate(chess);
    var time = Date.now() - start;
    var bench2 = bench();
    var checkmateTimedOut = time >= 60000;
    chess.metadata.date = moment(start).format('YYYY.MM.DD');
    chess.metadata.white = '5D-Chess-DB-Gen';
    chess.metadata.black = '5D-Chess-DB-Gen';
    chess.metadata.result = checkmate ?
      chess.player === 'white' ?
        '0-1'
      :
        '1-0'
    : chess.isStalemate ?
      '1/2-1/2'
    :
      '*';
    chess.metadata.bench_time = (bench1 + bench2)/2;
    chess.metadata.checkmate_time = time;
    chess.metadata.checkmate_difficulty = (time)/((bench1 + bench2)/2);
    chess.metadata.checkmate_timeout = checkmateTimedOut ? 'true' : 'false'
    chess.metadata.node = process.versions.node;
    chess.metadata.v8 = process.versions.v8;
    chess.metadata.hash = chess.hash;
    chess.metadata.chessjs = pjson.dependencies['5d-chess-js'];
    res.path += checkmate ?
      chess.player
    : chess.isStalemate ?
      'stalemate'
    :
      'none';
    if(checkmateTimedOut) {
      res.path += '_timeout';
    }
    res.filename = chess.hash + '-' + start + '.5dpgn';
    res.data = chess.export();
    return res;
  }
});
