const Chess = require('5d-chess-js');
const bruteCheckmate = require('./bruteCheckmate');

module.exports = () => {
  var chess = new Chess();
  chess.import('[Board "Standard"]\n' +
  '[Mode "5D"]\n' +
  '1. e3 / f6 2. Qe2 / Nc6 3. Qh5', false);
  //Warmup
  for(var i = 0;i < 5;i++) {
    bruteCheckmate(chess);
  }
  var start = Date.now();
  for(var i = 0;i < 10;i++) {
    bruteCheckmate(chess);
  }
  return (Date.now() - start)/10;
};
