# 5D Chess DB

Checkmate database of '5D Chess With Multiverse Time Travel' (using 5d-chess-js library) for bots, engines, and other algorithm research purposes.

## Access

Request access to the community contributed database by joining the Open 5D Chess discord and asking (http://discord.chessin5d.net).

To grab the database, use `pull.bat` (Windows) or `pull.sh` (Linux) with `gsutil` installed.

Shad Amethyst has kindly provided a public endpoint on his site here: http://shadamethyst.xyz/mirror/5d-chess-db.tar.gz

He has also made a json converted version available here: http://shadamethyst.xyz/mirror/5d-chess-db-converted.tar.gz

## Contribute

Help contribute to the database by donating your computer power to generating games. See installation section below on how to get started. You will need access to contribute.

## Installation

Before you can contribute to the database, you will need to install `gsutil` found in the GCloud SDK (installation instructions here: https://cloud.google.com/storage/docs/gsutil_install#install).

Once this is done, you have two ways of running the program:

**1. Use the prebuilt executable found here: https://storage.googleapis.com/db-chess-in-5d/build.zip**

  - SHA-1 Checksum is D1D1BAEE657FD71866C780F148427708CA4B8077
  - Use `start.bat` (Windows) or `start.sh` (Linux) to start both the code and auto-sync process
  
**2. Run from source code via NPM (Assuming Node and NPM is install)**
  
  - Run `npm install`
  - Use `start.bat` (Windows) or `start.sh` (Linux) to start both the code and auto-sync process
  - Run `npm run dbgen` to start without the auto-sync process

## Usage / DB Structure

The database is organized in the following structure:

```
db / <Variant> / <[ white | black | stalemate | none ] [ _timeout ]> / <hash>-<unix-date>.5dpgn
         ^                        ^                         ^             ^           ^
   variant string         checkmate indicator      timeout indicator  board hash  unix timestamp
```

Let's take a look at an example (concatenated for space):

```
db/standard/white/8abb0aa3f65f1220fee966da7f5d717c-1611640107457.5dpgn

[Board "Standard"]
[Mode "5D"]
[Date "2021.01.26"]
[White "5D-Chess-DB-Gen"]
[Black "5D-Chess-DB-Gen"]
[Result "0-1"]
[Bench_time "52.95"]
[Checkmate_time "60045"]
[Checkmate_difficulty "1133.9943342776203"]
[Checkmate_timeout "false"]
[Node "12.18.1"]
[V8 "7.8.279.23-node.38"]
[Hash "8abb0aa3f65f1220fee966da7f5d717c"]
[Chessjs "^1.0.2"]
1. d4 / Nh6
2. b4 / a6
3. Kd2 / b5
4. (0T4)Qd1>>(0T2)d3 / (1T2)b6
5. (1T3)Qd3>>(0T2)e4 / (1T3)Bf8>>(1T2)g8 (2T2)Rg8
6. (-1T3)Ke1>>(0T4)e1 (2T3)Qe4>(1T4)d3 / (2T3)Nc6 (-1T3)Ng4
7. (2T4)Nb1>>(1T4)b3 (-1T4)Qd3>>(0T3)e3 / (2T4)e6 (1T4)O-O (0T4)f6 (-1T4)Nxf2
8. (-1T5)g4 (1T5)Q3d2 (0T5)Kd1 (2T5)c3 / (1T5)e5 (-1T5)Nc6 (2T5)Nc6>>(2T3)b6
9. (-2T4)f3 / (3T4)Nb8>>(1T4)b7 (4T4)e6 (-2T4)Nb8>>(-1T4)b6 (5T3)b6
10. (5T4)Qed2 / (5T4)Nh6>>(4T4)f6
11. (4T5)Bxh6 (3T5)h3 (-5T5)Kd2 (5T5)Qd2>>x(1T5)h6 (-3T5)Qd3>(-4T5)e4 (-2T5)Ke1>(-1T6)d2 / (-3T5)e6 (6T5)Ba6 (0T5)Bf8>>(1T5)e8 (-2T5)Nc4 (-5T5)Nh6>>(-3T5)h5 (3T5)Pb5>(5T5)b5 (-4T5)Ng4>>(-5T5)g6 (4T5)Nb8>>(6T5)b7
12. (-6T6)Qd2>(-7T6)c3 (-5T6)h3 (1T6)b3 (-2T6)Nd2 (-3T6)g4 (-4T6)Qe4>>(-5T5)e4 / (7T5)Nxe4
13. (5T6)Ke1>>(4T5)d2 / (8T5)Rg8
14. (4T6)Pb2>(3T6)b2 (-9T6)Qdh3 (0T6)Pf2>>x(-1T5)f2 / (9T5)h6
15. (2T6)Qb3 (6T6)Qd3>(9T6)g3 (8T6)c4 (7T6)Ng1>>(6T6)g3 (-8T6)Kd2>>(-9T6)c3 / (8T6)Ke7 (7T6)Nd6 (-8T6)Nf6>(-9T6)f4 (-2T6)Nc4>>x(-2T4)d4
16. (-10T5)Be3 / (-10T5)Nxe2
...
```

There are a couple of metadata tags to look for:

 - Result - Follows PGN seven roster format (white score - black score)
 - Bench_time - Average time in milliseconds to run checkmate detection on a known fixed game (higher number indicates slower cpu)
 - Checkmate_time - Time in milliseconds taken to run checkmate detection on this game
 - Checkmate_difficulty - Checkmate_time / Bench_time (used to rate how computationally difficult this game is)
 - Checkmate_timeout - Boolean string indicating if checkmate detection algorithm timed out (checkmate is not confirmed)
 - Node - Nodejs version used
 - V8 - V8 engine version used
 - Hash - Board hash
 - Chessjs - 5d-chess-js version

## Bench

To allow for variety of hardware to contribute, a fixed game is used to benchmark the checkmate algorithm so there is a CPU independent difficulty score.

The game used for the benchmark is:

```
[Board "Standard"]
[Mode "5D"]
1. e3 / f6
2. Qe2 / Nc6
3. Qh5
```

## Depreciated DB

The old style DB with notation is available here (not updated): https://storage.googleapis.com/db-chess-in-5d/db_old.tar.gz

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
