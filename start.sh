#!/usr/bin/env bash

if [ -f "5d-chess-db-linux" ]
then
    echo "Running Executable"
    ./5d-chess-db-linux &
else 
    echo "Running Using NPM"
    npm run dbgen &
fi

mkdir db
mkdir db_pull
while [ true ]
do
  echo "Pushing to Cloud"
  
  gsutil -m mv -ce ./db/ gs://db-chess-in-5d/
  
  sleep 3600

done